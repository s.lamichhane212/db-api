const pool = require("./db");
const query = require("./query");

const postTodo = async(req,res)=>{
    try {
        const {description} = req.body;
        const newTodo = await pool.query(query.postTodo,[description]);

    res.json(newTodo.rows[0]);

    } catch (err) {
        console.error(err.message);
    }
}

const getTodo = async(req,res)=>{
    try {
        const allTodo = await pool.query(query.getTodo);

    res.json(allTodo.rows);

    } catch (err) {
        console.error(err.message);
    }
}

const getparticularTodo =async(req,res)=>{
    try {
        const { id } = req.params; 
        const Todo = await pool.query(query.getparticularTodo,[id]);

    res.json(Todo.rows[0]);

    } catch (err) {
        console.error(err.message);
    }
}

const putTodo = async(req,res)=>{
    try {
        const { id } = req.params; 
        const {description} = req.body;
        const updateTodo = await pool.query(query.putTodo,[description, id]);

    res.json("Todo was updated");

    } catch (err) {
        console.error(err.message);
    }
}

const deleteTodo = async(req,res)=>{
    try {
        const { id } = req.params; 
        const deleteTodo = await pool.query(query.deleteTodo,[id]);

    res.json("TODO was deleted");

    } catch (err) {
        console.error(err.message);
    }
}

module.exports ={
    postTodo,getTodo,getparticularTodo,putTodo,deleteTodo
};