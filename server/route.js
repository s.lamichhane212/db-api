const {Router} = require('express');
const controller = require('./controller');
const router = Router();


router.post("/todos", controller.postTodo)

//get all todoes 
router.get("/todos", controller.getTodo)

router.get("/todos/:id",controller.getparticularTodo)

//update a todo
router.put("/todos/:id", controller.putTodo) 

//delete a todo
router.delete("/todos/:id",controller.deleteTodo)

module.exports = router;