const pool = require("./db");

const postTodo = "INSERT INTO todo (description) VALUES($1) RETURNING *";

const getTodo = "SELECT * FROM todo";

const getparticularTodo = "SELECT * FROM todo WHERE todo_id =$1";

const putTodo = "UPDATE todo SET description = $1 WHERE todo_id = $2";

const deleteTodo = "DELETE FROM todo WHERE todo_id =$1";

module.exports ={
    postTodo,getTodo,getparticularTodo,putTodo,deleteTodo
};